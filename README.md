

## Getting started

```bash
# clone the project
git clone https://github.com/PanJiaChen/vue-element-admin.git

# enter the project directory
cd vue-element-admin

# install dependency
npm install

# develop
npm run dev
```

This will automatically open http://localhost:9527

## Build

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```

## Advanced

```bash
# preview the release environment effect
npm run preview

# preview the release environment effect + static resource analysis
npm run preview -- --report

# code format check
npm run lint

# code format check and auto fix
npm run lint -- --fix
```



## nginx 配置文件demo见src目录下面

```bash
ubuntu:
/usr/sbin/nginx：主程序
/etc/nginx：存放配置文件
/usr/share/nginx：存放静态文件 /usr/share/nginx
/var/log/nginx：存放日志
mac:
主页的文件在/usr/local/var/www 文件夹下
对应的配置文件地址在/usr/local/etc/nginx/nginx.conf

重启nginx
nginx -s reload
```

...





